

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class BasicObjectServlet
 */
@WebServlet("/BasicObjectServlet")
public class BasicObjectServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.OOAssocCode assosciatedCode;

    public BasicObjectServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.OOAssocCode(); 		//new assoc code
        
        //adding to the initial code
        assosciatedCode.addToInitial("public class dish {");
        assosciatedCode.addToInitial("    private String name;");
        assosciatedCode.addToInitial("    private Double price;");
        assosciatedCode.addToInitial("    private String course;");
        assosciatedCode.addToInitial("    private int calories;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish() {}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToInitial("    public String getName() { return name; }");
        assosciatedCode.addToInitial("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToInitial("    public Double getPrice() { return price; }");
        assosciatedCode.addToInitial("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToInitial("    public String getCourse() { return course; }");
        assosciatedCode.addToInitial("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToInitial("    public int getCalories() { return calories; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public static void main(String[] args) {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("        dish d = new dish();");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Basic Objects</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Basic Objects</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Object Orientated programming (OO) is one of the most well known and used programming styles.</p>"
					+ "<p>One of the things that OO allows us to do is reduce the amount of code we have to type out.</p>"
					+ "<p>An easy way to think about OO programming is to think of cookie cutters. In a class we define the template for whatever it is we want in our program, e.g. students, cars, animals, etc. In a class we store two things; attributes and methods, but in this case we will only look at classes with atttributes. Every time that we need to make a new item of the type we simply make a new instance of the class, a new object, saving time spent having to re-write the items each time from scratch and erors that can appear in the code from improper re-writes.</p>"
					+ "<p>Each of these classes becomes an object and can be used almost like a data type within our programs.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>Imagine we want to create a menu. A menu is just a list of dishes. So for this system we want a class called \"dish\" that holds the details of each dish on the menu.</p>"
					+ "<p>To work out a classes attributes we need to think about what it is we want to store about the classes subject, what they all have in common.</p>"
					+ "<p>For the case of a dish on a menu, they all have: <ul><li>a name</li><li>a price</li><li>what course it belongs to (starter, main or desert)</li><li>calorie count (if that's your thing!)</li></ul></p>"
					+ "<p>Every time the system needs a new dish we simply create a new instance of the \"dish\" class. This will automatically allow us to add all the neccessary details to this new item and know it is set up in the exact same way as all the other dishes.</p>"
					+ "<p>Whenever we set up a class we need to have certain things included. We need getters and setters, which ... get and set the attributes! These allow us to alter or retrieve the values of the attributes of the class. The getters returns the value in the same data type as the attribute. The setter takes in a value of the same data type as the attribute and sets the attribute to equal the value passed in.</p>"
					+ "<p>Lastly we need to include a constructor which allows us make the objects. This simply takes the form of the <b> public className () {} </b></p>"
					+ "<p>We can make more constructors that take in data and set attributes using it, or they can perform actions within the {}. But we'll just stick to the simple constructor for now.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Below is written the code to create the dish class.</p>"
					+ "<p>Before adding to it, can you understand what is going on?<ul><li>The class name is at the top</li>Then the variables the class stores, which are private to the class</li><li>The generic constructor that takes nothing in</li><li>The getters and setters, each with getVariableName and setVariableName</li></ul></p>"
					+ "<p>The code beneath the class decleration <b>public static void main(String[] args)</b> is the entry point for the program. Think of it like a door, when the code is compiled and run it looks for the door to know where to enter the code at ... this line shows it!</p>"
					+ "<p>The menu now includes <b>Steak and Chips</b> which costs <b>14.99</b> is a <b>Main</b> course and has <b>1000</b> calories.</p>"
					+ "<p>A basic dish type has been created. Using the setters, fill this dishes information. Then output the following <b>getName() costs getPrice()</b></p>"
					+ "<p>HINT : To use setters we use objectrName.setName(\"name\"); In this example d.setName(\"Steak and Chips\");</p>"
					+ "<p>Finally, please don't use comments after each line or add blank lines!</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='30'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("BasicObject", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("public class dish {");
        assosciatedCode.addToCorrect("    private String name;");
        assosciatedCode.addToCorrect("    private Double price;");
        assosciatedCode.addToCorrect("    private String course;");
        assosciatedCode.addToCorrect("    private int calories;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish() {}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToCorrect("    public String getName() { return name; }");
        assosciatedCode.addToCorrect("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToCorrect("    public Double getPrice() { return price; }");
        assosciatedCode.addToCorrect("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToCorrect("    public String getCourse() { return course; }");
        assosciatedCode.addToCorrect("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToCorrect("    public int getCalories() { return calories; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public static void main(String[] args) {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        dish d = new dish();");
        assosciatedCode.addToCorrect("        d.setName(\"Steak and Chips\");");
        assosciatedCode.addToCorrect("        d.setPrice(14.99);");
        assosciatedCode.addToCorrect("        d.setCourse(\"Main\");");
        assosciatedCode.addToCorrect("        d.setCalories(1000);");
        assosciatedCode.addToCorrect("        System.out.println(d.getName() + \" costs \" + d.getPrice());");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist			
		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//check for the correct amount of code
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
				
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check current line against the correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
	
			if (assosciatedCode.jDoodleTest("").toString().equals("Steak and Chips costs 14.99\\n")) 		//check if the output is steak and chips costs 14.99
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 			//set the fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for each test run
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if failed then increase the counter
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if no fails then the challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
