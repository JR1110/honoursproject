package assosciatedCode;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class AssocCode {

	private ArrayList<String> initialCode; 				//arraylist to hold any code that should appear upon loading
	private ArrayList<String> studentAnswer;			//arraylist to hold the students answer to the code challenge
	private ArrayList<String> correctAnswer;			//arraylist to hold the correct answer for the challenge
	
	public AssocCode() {
		//setting up and initialising the initial code for the student
		initialCode = new ArrayList<String>();
		initialCode.add("//This is a comment, enter your code below here and we can test it!");
		
		correctAnswer = new ArrayList<String>();
		correctAnswer.add("//This is a comment, enter your code below here and we can test it!");
		
		//setting up the students answer to be blank
		studentAnswer = new ArrayList<String>();
	}
	
	//method to add a line to the initial code that appears on page load
	public void addToInitial(String toAdd)
	{
		this.initialCode.add(toAdd);
	}
	
	//method to add a line to the correct answer to be checked against
	public void addToCorrect(String toAdd)
	{
		this.correctAnswer.add(toAdd);
	}
	
	//method for sending to JDoodle 
	public String jDoodleTest(String importStatements)
	{
		String response = ""; 		//string to send as response
		
		/*
		 * Sending the student's code to be able to be executed
		 * Code used in this provided from jdoodle API Docs (https://www.jdoodle.com/compiler-api/docs)
		*/
		String clientID = "82f0f5f11bc2475bfb40c5023147406d";
		String clientSecret = "320c3ff8db2b2b67073c0a8f70248ea6265fbe62cb4359228364e7f4d04d54bf";
		String script = importStatements + " public class test { public static void main (String[] args) { "; 
		String language = "java";
		String versionIndex = "0";
		
		// code to get the students answer to be the script sent through the API
		for (String s : this.studentAnswer)
		{
			if (!(s.startsWith("//")))
			{
				String cleaned = s.replace("\'","\\\"").replace("\"", "\\\"").replace("\\\\", "\\");
				script = script + cleaned;
			}
		}

		script = script + "}}";
		
		script = script.replaceAll("\n", " ").replaceAll("\r", ""); 		//replacing all the new lines with a single space
		
		String codeOutput = ""; 		//setting up a string to save the code output to
		
		try 
		{
			URL url = new URL("https://api.jdoodle.com/v1/execute");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            
            //set the relevant settings for the API connection
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
			
			String input = "{\"clientId\": \"" + clientID + "\",\"clientSecret\":\"" + clientSecret + "\",\"script\":\"" + script + "\",\"language\":\"" + language + "\",\"versionIndex\":\"" + versionIndex + "\"} "; 		//creates the string to send to JDoodle

		    System.out.println(input);

		    //setting up the output stream and writing to the URL
		    OutputStream outputStream = connection.getOutputStream();
            outputStream.write(input.getBytes());
            outputStream.flush();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Please check your inputs : HTTP error code : "+ connection.getResponseCode()); 		//throws error if one exists
            }

            //setting up the reader back from JDoodle
            BufferedReader bufferedReader;
            bufferedReader = new BufferedReader(new InputStreamReader(
            (connection.getInputStream())));

            String output;
            System.out.println("Output from JDoodle .... \n");
            while ((output = bufferedReader.readLine()) != null) {
                System.out.println(output);
                codeOutput = codeOutput + output;
            }

            connection.disconnect(); 			//disconnect from the API

			
		} catch (IOException e) { 			//catch any IO exceptions
			e.printStackTrace();
		}
		
		String[] outputs = codeOutput.split(",");	 		//splitting the outputs on the comma
		
		for (String s : outputs) 							//looping through the outputs
		{
			if (s.contains("output")) 				//if the output starts with output
			{
				String[] split = s.split(":"); 				//split it on the :
				response = split[1].replaceAll("\"","");	//set the response to be the second one, the value of the JSON output
			}
		}
		
		return response; 			//returns the output
	}
	
	//GETTERS AND SETTERS
	
	public ArrayList<String> getInitialCode() {
		return initialCode;
	}
	public void setInitialCode(ArrayList<String> initialCode) {
		this.initialCode = initialCode;
	}
	public ArrayList<String> getStudentAnswer() {
		return studentAnswer;
	}
	public void setStudentAnswer(ArrayList<String> studentAnswer) {
		this.studentAnswer = studentAnswer;
	}

	public ArrayList<String> getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(ArrayList<String> correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	
}


