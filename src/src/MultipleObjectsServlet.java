

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MultipleObjectsServlet
 */
@WebServlet("/MultipleObjectsServlet")
public class MultipleObjectsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.OOAssocCode assosciatedCode;

    public MultipleObjectsServlet() {
        super();
        testResults = new TestResults(); 								//new test results
        assosciatedCode = new assosciatedCode.OOAssocCode(); 			//new assoc code
        
        //adding to the initialised code
        assosciatedCode.addToInitial("import java.util.ArrayList;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("public class dish {");
        assosciatedCode.addToInitial("    private String name;");
        assosciatedCode.addToInitial("    private Double price;");
        assosciatedCode.addToInitial("    private String course;");
        assosciatedCode.addToInitial("    private int calories;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish() {}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        this.name = name;");
        assosciatedCode.addToInitial("        this.price = price;");
        assosciatedCode.addToInitial("        this.course = course;");
        assosciatedCode.addToInitial("        this.calories = calories;");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToInitial("    public String getName() { return name; }");
        assosciatedCode.addToInitial("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToInitial("    public Double getPrice() { return price; }");
        assosciatedCode.addToInitial("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToInitial("    public String getCourse() { return course; }");
        assosciatedCode.addToInitial("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToInitial("    public int getCalories() { return calories; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void outputDish()");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public static void main(String[] args) {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Multiples of an Object</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Multiples of an Object</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Building on our menu, we want to have multiple dishes on it!</p>"
					+ "<p>With the class, constructors and methods now added to the dish class we are in a position to create multiple of them to start making up the menu.</p>"
					+ "<p>This could be done, commonly, in three ways. We can either make an object per each dish, as if it was a data type. We could use an array of the dishes. Or we could create an arraylist that contains our class, dish, as the type held within.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>To make multiple dishes all we need to do is make a new dish and ensure we change the name of the next one!<br><b>dish d1 = new dish();<br>...<br>dish d2 = new dish(\"Lasagne\",13.98,\"Main\",830);</b></p>"
					+ "<p>Or we could make an array<br><b>dish[] menu = new dish[3]<br>menu[0] = new dish(\"Burger\",12.00,\"Main\",1200);</b></p>"
					+ "<p>And the final, and simplest way is to make an array list of dish type<br><b>ArrayList&lt;dish&gt; menu = new ArrayList&lt;dish&gt;(); <br> menu.add(new dish(\"Breaded Brie\",4.99,\"Starter\",480));</b></p>"
					+ "<p>If using array lists, just remember to include the import statement for it!</p>"
					+ "<p></p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>We are continuing to adapt the dish class.</p>"
					+ "<p>We are going to make the menu by making an array list of dishes, called \"menu\".</p>"
					+ "<p>Create this array list and add <b>4</b> items to it.</p>"
					+ "<p>Add these items as shown in the example above, using the constructor that takes in values and sets the dish from them.</p>"
					+ "<p>Finally output the length of the arraylist in order to show it contains 4 dishes. If you can't remember how to do this then have a look back at the ArrayLists challenge.</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='40'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("MultipleObjects", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution in the assoc code
	private void correctSolution()
	{
        assosciatedCode.addToCorrect("import java.util.ArrayList;");
        assosciatedCode.addToCorrect(" ");
		assosciatedCode.addToCorrect("public class dish {");
        assosciatedCode.addToCorrect("    private String name;");
        assosciatedCode.addToCorrect("    private Double price;");
        assosciatedCode.addToCorrect("    private String course;");
        assosciatedCode.addToCorrect("    private int calories;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish() {}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        this.name = name;");
        assosciatedCode.addToCorrect("        this.price = price;");
        assosciatedCode.addToCorrect("        this.course = course;");
        assosciatedCode.addToCorrect("        this.calories = calories;");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToCorrect("    public String getName() { return name; }");
        assosciatedCode.addToCorrect("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToCorrect("    public Double getPrice() { return price; }");
        assosciatedCode.addToCorrect("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToCorrect("    public String getCourse() { return course; }");
        assosciatedCode.addToCorrect("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToCorrect("    public int getCalories() { return calories; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void outputDish()");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public static void main(String[] args) {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        ArrayList<dish> menu = new ArrayList<dish>();");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"");
        assosciatedCode.addToCorrect("       System.out.println(menu.size());");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist			
		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check for right number of lines present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 				//if so it has passed that test
				
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check current line against correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
	
			String runCheck = assosciatedCode.jDoodleTest("").replaceAll("\\\\n", ""); 		//string to hold the output of the code
			
			if ((!(runCheck.contains("/test.java"))) && runCheck.contains("4") && runCheck.contains("has been created")) 		//if no JDoodle error present, the output contains the number 4 and has been created then it has passed
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 			//for all the tests ran on the code
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 			//if it has failed then add to the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if none have failed then the whole challenge has been passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
