

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SectionSelecter
 */
@WebServlet("/SectionSelecterServlet")
public class SectionSelecterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public SectionSelecterServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		try {
			
			response.setContentType("text/html"); 					//setting the content type to html
			
			PrintWriter writer = response.getWriter(); 				//setting up the writer
			
			//writing the head
			writer.println("<!DOCTYPE html>\r\n" + 
					"<html lang=\"en\">\r\n" + 
					"    <head>\r\n" + 
					"        <meta charset=\"UTF-8\">\r\n" + 
					"        <title>Section Selector</title>\r\n" + 
					"        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\r\n" + 
					"        <link rel=\"stylesheet\" href=\"custom.css\">\r\n" + 
					"        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>\r\n" + 
					"        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\r\n" + 
					"    </head>");
			
			//writing the table containing all the challenges, if they have been passed by the session vlaue then make it appear green!
			writer.println("<body>\r\n" + 
					"    	<div class=\"container\">\r\n" + 
					"	        <h1>Select area to study</h1>\r\n" + 
					"	        <div class=\"selector\">\r\n" + 
					"	            <table id=\"subjectSelector\" class=\"table\">\r\n" + 
					"	                <tr class='info'>\r\n" + 
					"	                    <th colspan=\"3\"><h4>Data Types and Structures</h4></th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"subSubject\">\r\n" + 
					"	                    <th colspan=\"3\">Strings</th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("HelloWorld") != null && session.getAttribute("HelloWorld").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href=\"HelloWorldServlet\">Hello World</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("StoringInfoInStrings") != null && session.getAttribute("StoringInfoInStrings").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='StoringStringsServlet'>Storing Information in Strings</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ManipulatingStrings") != null && session.getAttribute("ManipulatingStrings").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ManipulatingStringsServlet'>Manipulating and Altering Strings</a></td>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"subSubject\">\r\n" + 
					"	                    <th colspan=\"3\">Numbers</th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("Integers") != null && session.getAttribute("Integers").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='IntegerServlet'>Integers (Whole Numbers)</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("DoubleAndFloat") != null && session.getAttribute("DoubleAndFloat").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='DoubleServlet'> Double and Float (Decimal Points)</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ConvertingNumbers") != null && session.getAttribute("ConvertingNumbers").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ConvertingNumbersServlet'>Converting and Using Numbers</a></td>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"subSubject\">\r\n" + 
					"	                    <th colspan=\"3\">Boolean</th>\r\n" + 
					"	                </tr>\r\n"+ 
					"					<tr class=\"challenges\">\r\n" +
					"  						<td colspan='3'");
			if (session.getAttribute("Boolean") != null && session.getAttribute("Boolean").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='BooleanServlet'> Boolean </a></td></tr>\r\n" +
					"	                <tr class=\"subSubject\">\r\n" + 
					"	                    <th colspan=\"3\">Storing Information</th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("Arrays") != null && session.getAttribute("Arrays").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print(" ><a href=\"ArrayServlet\">Arrays</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ArrayLists") != null && session.getAttribute("ArrayLists").equals("Passed!"))
			{
				writer.print(" class='success'");
			}
			writer.print(" colspan='2'><a href=\"ArrayListsServlet\">ArrayLists</a></td>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class='info'>\r\n" + 
					"	                    <th colspan=\"3\"><h4>Computational Constructs</h4></th>\r\n" + 
					"	                </tr>\r\n" +
					"  					<tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("IfStatements") != null && session.getAttribute("IfStatements").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href=\"IfStatementServlet\">If Statements</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ForLoopInt") != null && session.getAttribute("ForLoopInt").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ForLoopIntServlet'>For Loops (Using Integers)</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ForLoopVariable") != null && session.getAttribute("ForLoopVariable").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ForLoopVariableServlet'>For Loops (Using Variables)</a></td>\r\n" + 
					"	                </tr>\r\n" +
					"	                <tr class='info'>\r\n" + 
					"	                    <th colspan=\"3\"><h4>\"Classic\" Algorithms</h4></th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("MaxAndMin") != null && session.getAttribute("MaxAndMin").equals("Passed!"))
			{ 
				writer.print(" class='success' ");
			}
			writer.print("><a href='MaxAndMinServlet'>Max and Min</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("Searching") != null && session.getAttribute("Searching").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='SearchingServlet'>Searching</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("CountingOccurrences") != null && session.getAttribute("CountingOccurrences").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='CountingOccurrencesServlet'>Counting Occurrences</a></td>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class='info'>\r\n" + 
					"	                    <th colspan=\"3\"><h4>Object Orientation</h4></th>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("BasicObject") != null && session.getAttribute("BasicObject").equals("Passed!"))
			{ 
				writer.print(" class='success' ");
			}
			writer.print("><a href='BasicObjectServlet'>Basic Objects</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ObjectsWithMethods") != null && session.getAttribute("ObjectsWithMethods").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ObjectsWithMethodsServlet'>Objects with Methods</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("ObjectWithAdvancedConstructor") != null && session.getAttribute("ObjectWithAdvancedConstructor").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='ObjectWithAdvancedConstructorServlet'>Objects with Advanced Constructors</a></td>\r\n" + 
					"	                </tr>\r\n" + 
					"	                <tr class=\"challenges\">\r\n" + 
					"	                    <td");
			if (session.getAttribute("MultipleObjects") != null && session.getAttribute("MultipleObjects").equals("Passed!"))
			{ 
				writer.print(" class='success' ");
			}
			writer.print("><a href='MultipleObjectsServlet'>Multiples of an Object</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("FindingOnMenu") != null && session.getAttribute("FindingOnMenu").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='FindingOnMenuServlet'>Dealing with Multiple Objects</a></td>\r\n" + 
					"	                    <td");
			if (session.getAttribute("Inheritance") != null && session.getAttribute("Inheritance").equals("Passed!"))
			{
				writer.print(" class='success' ");
			}
			writer.print("><a href='InheritanceServlet'>Inheritance</a></td>\r\n" + 
					"	                </tr>\r\n" +
					"	            </table>\r\n" + 
					"	        </div>\r\n" + 
					"        </div>\r\n" + 
					"    </body>\r\n" + 
					"</html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


}
