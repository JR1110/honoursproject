

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ObjectWithAdvancedConstructorServlet
 */
@WebServlet("/ObjectWithAdvancedConstructorServlet")
public class ObjectWithAdvancedConstructorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.OOAssocCode assosciatedCode;

    public ObjectWithAdvancedConstructorServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.OOAssocCode(); 		//new assosciated code
        
        //adding code into the initial code for the challenge
        assosciatedCode.addToInitial("public class dish {");
        assosciatedCode.addToInitial("    private String name;");
        assosciatedCode.addToInitial("    private Double price;");
        assosciatedCode.addToInitial("    private String course;");
        assosciatedCode.addToInitial("    private int calories;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish() {}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToInitial("    public String getName() { return name; }");
        assosciatedCode.addToInitial("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToInitial("    public Double getPrice() { return price; }");
        assosciatedCode.addToInitial("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToInitial("    public String getCourse() { return course; }");
        assosciatedCode.addToInitial("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToInitial("    public int getCalories() { return calories; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void outputDish()");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public static void main(String[] args) {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Objects With Advanced Constructors</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Objects With Advanced Constructors</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>As mentioned initially, we can create specific constructors that can take in values and create the object based on them.</p>"
					+ "<p>These constructors can take in as much or as little as we want but they will always create a new object based on our class.</p>"
					+ "<p>We can even ask the constructor to do other tasks as well as we create the object ... even call class methods!</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>This saves us setting all the variables individually and allows us to make an object and set it's value all in the one line.</p>"
					+ "<p>Like the standard constructor we need to call the constructor by the name of the class. Unlike the standard constructor we take in variables in the brackets and need to actually put something in between the curly brakets {}.</p>"
					+ "<p>Then we can set the class attributes useing the items we have passed into the constructor.</p>"
					+ "<p>This is done in the same way as the setter using <b>this.attribute = valuePassedIn;</b> For example : </p>"
					+ "<p>public dish(String name)<br>{<br>&emsp;this.name = name;<br>}</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>We are continuing to adapt the dish class.</p>"
					+ "<p>The start to a constructor that takes in values is shown.</p>"
					+ "<p>Finish the constructor so that each of the attributes is set. Then at the end of the constructor output <b>name has been created</b></p>"
					+ "<p>Once this has been done. Create a new dish in one line using the new constructor. Finally, output the dish using the method created</p>"
					+ "<p>HINT : To pass in multiple values to the constructor, use a comma to seperate them and simply type in the values as you would for a setter in the previous examples.</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='30'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("ObjectWithAdvancedConstructor", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding code into the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("public class dish {");
        assosciatedCode.addToCorrect("    private String name;");
        assosciatedCode.addToCorrect("    private Double price;");
        assosciatedCode.addToCorrect("    private String course;");
        assosciatedCode.addToCorrect("    private int calories;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish() {}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        this.name = name;");
        assosciatedCode.addToCorrect("        this.price = price;");
        assosciatedCode.addToCorrect("        this.course = course;");
        assosciatedCode.addToCorrect("        this.calories = calories;");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToCorrect("    public String getName() { return name; }");
        assosciatedCode.addToCorrect("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToCorrect("    public Double getPrice() { return price; }");
        assosciatedCode.addToCorrect("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToCorrect("    public String getCourse() { return course; }");
        assosciatedCode.addToCorrect("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToCorrect("    public int getCalories() { return calories; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void outputDish()");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public static void main(String[] args) {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        dish d = new dish(");
        assosciatedCode.addToCorrect("        d.outputDish();");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist			
		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//check for enough lines  present
		{ 
			testResults.addResult("Correct Number Of Lines", "Passed!"); 				//pass that test
				
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check the code against the correct answer bar spaces
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
	
			String runCheck = assosciatedCode.jDoodleTest("").replaceAll("\\\\n", ""); 		//string to hold the output of the code
			
			if ((!(runCheck.contains("/test.java"))) && runCheck.contains("costs") && runCheck.contains("has been created")) 				//if the has been created output is present and the JDoodle error message isnt then it has passed
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 			//setting the fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all the tests performed
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if the test is failed then add one to the counter
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if no tests have failed then the challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
