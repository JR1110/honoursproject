

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ForLoopVariableServlet
 */
@WebServlet("/ForLoopVariableServlet")
public class ForLoopVariableServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public ForLoopVariableServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.AssocCode();			//new assoc code
        assosciatedCode.addToInitial("String[] stringArray = {\"first\",\"second\",\"third\",\"fourth\"};"); 		//adding to the initial code
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>For Loops (Using Variables)</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>For Loops Using Variables</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>For loops can be an easy and powerful way to navigate through arrays and arraylists.</p>"
					+ "<p>instead of using integers to loop through these we can use the variables, or data, themselves which is both cleaner and allows direct access to the individual pieces of data held within the array or arraylist.</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>Unlike the previous for loops we dont have to declare a number and iterate until a point of our choosing. We declare an individual item of the same data type as the array/list and the loop starts at the starts and stops itself once it has looped through the last data element.</p>"
					+ "<p>A for loop going through a String ArrayList would look like</p>"
					+ "<p><b> for(String s : stringArray) <br>{<br> &emsp; System.out.println(s); <br> } </b></p>"
					+ "<p>Instead of declaring an integer we assign the next item to an item of the same data type. Each time this loop runs the next String in the arraylist becomes s and we can use it as a string itself.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Loop through the array list provided, using String s in a for loop, output the each item in the array</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("ForLoopVariable", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("String[] stringArray = {\"first\",\"second\",\"third\",\"fourth\"};");
		assosciatedCode.addToCorrect("for (String s: stringArray)");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("System.out.println(s);");
		assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist

		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check for the right amount of code
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check current line against correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
			
			if (assosciatedCode.jDoodleTest("").toString().contains("first\\nsecond\\nthird\\nfourth\\n")) 			//check for output of first second third fourth; if so then passed test
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 			//sets fail count to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all the tests ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if it is failed, increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if nothing is failed then the challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}