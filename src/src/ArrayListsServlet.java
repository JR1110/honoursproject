

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/ArrayListsServlet")
public class ArrayListsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       

    public ArrayListsServlet() {
    	testResults = new TestResults(); 						//new test results
        assosciatedCode = new assosciatedCode.AssocCode();		//enw assoc code
        assosciatedCode.addToInitial("ArrayList<Integer> marks = new ArrayList<Integer>();"); 		//adding to the initial code
        correctSolution();  		//run the correct solution method
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>ArrayLists</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>ArrayLists</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>ArrayLists act a bit like arrays but allow different features and tools to be used.</p>"
					+ "<p>Like arrays, they store a list of multiple items of the same type. Unlike arrays they store an object not the data type itself. This is mostly seen in the way we set up an ArrayList but makes a subtle change to the way we access and manipulate the data inside of it</p>"
					+ "<p>ArrayLists however can be resized whenever new elements are put in so we aren't limited to the number of items as with arrays. This gives us mroe space to use but also takes more storage. On top of the storage concerns arraylists are also relatively slow to process in comparison to arrays.</p>"
					+ "<p>In Java we have to import ArrayLists though as they are not within the standard Java library. This is simple and can be done using <b>import java.util.ArrayList;</b></p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>An arraylist is defined as follows <b>ArrayList&lt;String&gt; shoppingList = new ArrayList&lt;String&gt;();</b></p>"
					+ "<p>In the declaration we use triangular brackets to define what object we are making a list of; String, Integer, etc. We can even make an ArrayList of a user's own object type  (we will discuss this in the Object Orientated Section).</p>"
					+ "<p>To add items into the ArrayList we use <b>.add()</b> and place the data inside as we would when asigning it to an item of that data type, \" for string and no \" for numbers for example.</p>"
					+ "<p>When we add it ends the data onto the end of the ArrayList. If we want to add it at a particular spot we can put a number into the add function which places it at the position  we want, e.g. <b>.add(1,\"xyz\");</b></p>"
					+ "<p>To see what is stored at an element in the ArrayList we use <b>.get()</b> inserting the position number that we want the element of.</p>"
					+ "<p>By using Collections within Java we can also shuffle, sort and do other functions to the list. We can also just naturally find out how big the list us using the </b>.size()</b> function</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>The ArrayList marks array has been made again. Insert the students marks; student 1 got 55, student 2 got 68 and student 3 got 41.</p>"
					+ "<p>Output the size of the ArrayList.</p>"
					+ "<p>Finally output the mark of the first student</p>"
					+ "<p>HINT : ArrayLists also start counting from 0!</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("ArrayLists", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("ArrayList<Integer> marks = new ArrayList<Integer>();");
		assosciatedCode.addToCorrect("marks.add(55);");
		assosciatedCode.addToCorrect("marks.add(68);");
		assosciatedCode.addToCorrect("marks.add(41);");
		assosciatedCode.addToCorrect("System.out.println(marks.size());");
		assosciatedCode.addToCorrect("System.out.println(marks.get(0));");
	}
	
	private void submitCode(String code)
	{
		int linesOfCode = 0; 									//int to hold the number of lines of code, not start like comment
		int semiColons = 0; 								//int to check how many semi colons have been entered
		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new ArrayList string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("\"", "'")); 										//add it to the arraylist
			
			if(!(s.contains("//") && (!(s.contains(";"))))) 		//if the line begins with a comment 
			{
				linesOfCode++;
			}
			
			if (s.contains(";")) 		//if it contains a semi colon
			{
				semiColons++; 								//adds one on to the semi colon count
			}
		}
		
		if (semiColons == linesOfCode) 			//if there are the same number of semi colons as lines of code
		{
			testResults.addResult("Contains Semi-Colons", "Passed!"); 		//adding the test result that the code contains relevant semi colons
		} else {
			testResults.addResult("Contains Semi-Colons", "Failed"); 		//adding the test results that it failed the semi colon check
		}

		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//if the correct number of lines have been enetered
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check against correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}

			if (assosciatedCode.jDoodleTest("import java.util.ArrayList;").toString().equals("3\\n55\\n")) 			//check the output is as requested (length: 3, returned: 55)
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set the fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for each test run
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if the test has failed then increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if no fails then challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}