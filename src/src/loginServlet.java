

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/loginServlet")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public loginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name"); 			//getting the new name
		
		HttpSession session = request.getSession(true); 		//getting the session
		
		if (name.equals("")) 								//if they are logged in as guest
		{
			session.setAttribute("mode", "G"); 					//set the mode to G -> Guest
			session.setAttribute("name", "guest");
		} else {
			session.setAttribute("mode", "U");					//set the mode to U -> User
			session.setAttribute("name", name); 				//set the name to the user's entered name
		}
		
		response.sendRedirect("SectionSelecterServlet"); 		//send the user to the section selector servlet
		
		
		
	}

}
