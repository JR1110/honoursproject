

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ArrayServlet
 */
@WebServlet("/ArrayServlet")
public class ArrayServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;

    public ArrayServlet() {
        super();
        testResults = new TestResults(); 								//new test results
        assosciatedCode = new assosciatedCode.AssocCode(); 				//new assoc code
        assosciatedCode.addToInitial("int[] marks = new int[3];"); 		//add to intiial code
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Arrays</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Arrays</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Arrays allow us to store multiple items of the same type, a collection of items of one data type.</p>"
					+ "<p>Like a shopping list that stores multiple string (the items on the list) we can use arrays to cleanly store multiple items of a certain type. This saves us having to make an individual item if we need lots of an item, for example we need lots of integers if we store a whole class worth of marks.</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>We will discuss 1D arrays. But you can also have more dimensions to arrays, but this gets very complicated very quickly!</p>"
					+ "<p>We have to set the size of an array from the start and arrays always start counting from number 0. This means that an array of size 3 would have items 0, 1 and 2.</p>"
					+ "<p>To declare an array in Java we state the array type then square brackets then the name of it</p>"
					+ "<p>We finish the decleration with one of two things, either by adding items into the array or by declaring the size of the array.</p>"
					+ "<p>A decleration of a string array with 3 elements looks like <b>String[] stringArray = new String[3];</b></p>"
					+ "<p> If we declare it like this we can then add to it using the element number we would like it to use - <b>stringArray[0] = \"first\";</b></p>"
					+ "<p>A decleration of a string array by stating the items in it would look like <b>String[] stringArray = {\"first\",\"second\",\"third\"};</b></p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>The array marks array has been made. Insert the students marks; student 1 got 55, student 2 got 68 and student 3 got 41.</p>"
					+ "<p>Output the mark that the second student got</p>"
					+ "<p>HINT : We can show the specific data stored in an array in the same way we add to it using square brackets []</p>"
					+ "<p>HINT : Remember that arrays start counting form 0</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("Arrays", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//add to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("int[] marks = new int[3];");
		assosciatedCode.addToCorrect("marks[0] = 55;");
		assosciatedCode.addToCorrect("marks[1] = 68;");
		assosciatedCode.addToCorrect("marks[2] = 41;");
		assosciatedCode.addToCorrect("System.out.println(marks[1]);");
	}
	
	private void submitCode(String code)
	{
		int linesOfCode = 0; 									//int to hold the number of lines of code, not start like comment
		int semiColons = 0; 								//int to check how many semi colons have been entered
		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("\"", "'")); 										//add it to the arraylist
			
			if(!(s.contains("//") && (!(s.contains(";"))))) 		//if the line begins with a comment 
			{
				linesOfCode++;
			}
			
			if (s.contains(";")) 		//if it contains a semi colon
			{
				semiColons++; 								//adds one on to the semi colon count
			}
		}
		
		if (semiColons == linesOfCode) 			//if there are the same number of semi colons as lines of code
		{
			testResults.addResult("Contains Semi-Colons", "Passed!"); 		//adding the test result that the code contains relevant semi colons
		} else {
			testResults.addResult("Contains Semi-Colons", "Failed"); 		//adding the test results that it failed the semi colon check
		}

		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//check for correct number of lines
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check current line against the correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}

			if (assosciatedCode.jDoodleTest("").toString().equals("68\\n")) 			//check if the output is 68
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 			//set the fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for each test ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if it is failed then increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 		//if no fails then the challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
