

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class BooleanServlet
 */
@WebServlet("/BooleanServlet")
public class BooleanServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public BooleanServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.AssocCode();			//new assoc code
        correctSolution(); 						//enter the correct solution
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}
	
	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Booleans</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Booleans</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Booleans are a really simple data type but also one of the most powerful ones when used right.</p>"
					+ "<p>Booleans take up minimal memory, 8 bytes in Java, and can only store two values; true or false.</p>"
					+ "<p>In high level languages we aare lucky to have them as true or false but in some lower level languages they are shown as 1 (true) or 0 (false) ... or even not there all together!</p>"
					+ "<p>They can be massively useful for making decisions within a programme, as well as providing a clean way of representing a simple fact or showing when a set criteria has been met. In fact to this website uses booleans to see whether or not you have passed each of the coding challenges.</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>As stated above, booleans simply hold either true or false. When set up and used correcvtly they can aid in loads of aspects of programming but mainly in if statements. We will loook at these later on!</p>"
					+ "<p>Like all data types, these are declared using their type then name then telling it to hold a value - <b>boolean isTrue = true;</b></p>"
					+ "<p>Because we are defining a boolean we don't need to put quotes around true or false, the program knows what we're talking about.</p>"
					+ "<p>These are not the most interesting by themselves but become really useful when used within decision making in programmes.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Seen as its very simple, make a boolean called isTrue and set it to be false</p>"
					+ "<p>Then use the system's output to display its value</p>"
					+ "<p>Hint: we can just stick the booleans name in the output and it'll show the value of it!</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("Boolean", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//add to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("boolean isTrue = false;");
		assosciatedCode.addToCorrect("System.out.println(isTrue);");
	}
	
	private void submitCode(String code)
	{
		int linesOfCode = 0; 									//int to hold the number of lines of code, not start like comment
		int semiColons = 0; 								//int to check how many semi colons have been entered
		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("\"", "'")); 										//add it to the arraylist
			
			if(!(s.contains("//") && (!(s.contains(";"))))) 		//if the line begins with a comment 
			{
				linesOfCode++;
			}
			
			if (s.contains(";")) 		//if it contains a semi colon
			{
				semiColons++; 								//adds one on to the semi colon count
			}
		}
		
		if (semiColons == linesOfCode) 			//if there are the same number of semi colons as lines of code
		{
			testResults.addResult("Contains Semi-Colons", "Passed!"); 		//adding the test result that the code contains relevant semi colons
		} else {
			testResults.addResult("Contains Semi-Colons", "Failed"); 		//adding the test results that it failed the semi colon check
		}

		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check for enough lines of code entered
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check the code against the correct asnwer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}

			if (assosciatedCode.jDoodleTest("").toString().equals("false\\n")) 				//check that the output is false
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all the test run
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if the test has been failed then increase the fail counter
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 		//if no fails then the challenge has been passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
