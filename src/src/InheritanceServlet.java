

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class InheritanceServlet
 */
@WebServlet("/InheritanceServlet")
public class InheritanceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.OOAssocCode assosciatedCode;

    public InheritanceServlet() {
        super();
        testResults = new TestResults(); 						//new test results
        assosciatedCode = new assosciatedCode.OOAssocCode(); 	//new assoc code
        
        //adding to the initial code
        assosciatedCode.addToInitial("public class dish {");
        assosciatedCode.addToInitial("    private String name;");
        assosciatedCode.addToInitial("    private Double price;");
        assosciatedCode.addToInitial("    private String course;");
        assosciatedCode.addToInitial("    private int calories;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish() {}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        this.name = name;");
        assosciatedCode.addToInitial("        this.price = price;");
        assosciatedCode.addToInitial("        this.course = course;");
        assosciatedCode.addToInitial("        this.calories = calories;");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToInitial("    public String getName() { return name; }");
        assosciatedCode.addToInitial("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToInitial("    public Double getPrice() { return price; }");
        assosciatedCode.addToInitial("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToInitial("    public String getCourse() { return course; }");
        assosciatedCode.addToInitial("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToInitial("    public int getCalories() { return calories; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void outputDish()");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public static void main(String[] args) {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("class special extends dish {");
        assosciatedCode.addToInitial("    private int quantity;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setQuantity(int quantity) { this.quantity = quantity; }");
        assosciatedCode.addToInitial("    public int getQuantity() { return quantity; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void sellOne()");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Inheritance</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Inheritance</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>The last and most complex area of OO programming we will look at is inheritance.</p>"
					+ "<p>Inheritance allows us to build onto our classes. It allows us to make a new class which inherits attributes and methods from another class. This means when we make a class that inherits from another, we can set attributes from the original class and use their methods whenever we make an instance of the new class.</p>"
					+ "<p>This is a useful concept because it allows to make base classes and build on them. Again saving time and effort in knowing that we can use methods and attributes from the original class in any of our inherited classes.</p>"
					+ "<p>The only thing we can't transfer across are the constructors. But this will all become clearer in the next sections.</p>"
					+ "<p>In Java, we simply say that class <b>className extends originalClassName</b> whenever we want to perform inheritance."
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>Say the menu needs specials put on it. These specials need to have a limited quantity and be able to remove one of their quantity when one is sold. Every other dish on the menu is normal and neever runs out, so has no need for quantities or the method of removing one.</p>"
					+ "<p>We could modify the class for dish and make every dish have this attribute and method. BUT this is adding stuff that isn't required in the majority of items required.</p>"
					+ "<p>Or we could make a whole new class could special that has the exact same attributes and methods. What is theres a spelling mistake in one, or an attribute is missing or simply a slight difference between the two.</p>"
					+ "<p>This is a perfect candidate for inheritance. If we use inheritance from the standard dish class then we can guarantee that we can still use name, price, course and calories as well as the outputDish() method. Then the only extra code we are required to write is that of making a quantity atttribute and writing the method for selling one.</p>"
					+ "<p>Then we just need to create a new special as we would a dish, or a data type.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>The special class has been created but needs the sellOne() method finished.</p>"
					+ "<p>The method should check if the quantity is less than 1 (no more left!). If it is: output \"No more left\". If it isn't then output \"New quantity is \" + quantity.</p>"
					+ "<p>Then within the main method. Create a new special. Set each of it's attributes (name, price, course, calories, quantity) and outputDish().</p>"
					+ "<p>Finally, sellOne() of the specials.</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='50'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("Inheritance", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("public class dish {");
        assosciatedCode.addToCorrect("    private String name;");
        assosciatedCode.addToCorrect("    private Double price;");
        assosciatedCode.addToCorrect("    private String course;");
        assosciatedCode.addToCorrect("    private int calories;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish() {}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        this.name = name;");
        assosciatedCode.addToCorrect("        this.price = price;");
        assosciatedCode.addToCorrect("        this.course = course;");
        assosciatedCode.addToCorrect("        this.calories = calories;");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToCorrect("    public String getName() { return name; }");
        assosciatedCode.addToCorrect("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToCorrect("    public Double getPrice() { return price; }");
        assosciatedCode.addToCorrect("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToCorrect("    public String getCourse() { return course; }");
        assosciatedCode.addToCorrect("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToCorrect("    public int getCalories() { return calories; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void outputDish()");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public static void main(String[] args) {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        special s = new special();");
        assosciatedCode.addToCorrect("        s.setName(\"");
        assosciatedCode.addToCorrect("        s.setPrice(");
        assosciatedCode.addToCorrect("        s.setCourse(\"");
        assosciatedCode.addToCorrect("        s.setCalories(");
        assosciatedCode.addToCorrect("        s.setQuantity(");
        assosciatedCode.addToCorrect("        s.outputDish();");
        assosciatedCode.addToCorrect("        s.sellOne();");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("class special extends dish {");
        assosciatedCode.addToCorrect("    private int quantity;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setQuantity(int quantity) { this.quantity = quantity; }");
        assosciatedCode.addToCorrect("    public int getQuantity() { return quantity; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void sellOne()");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        if (this.quantity < 1)");
        assosciatedCode.addToCorrect("        {");
        assosciatedCode.addToCorrect("            System.out.println(\"No more left\");");
        assosciatedCode.addToCorrect("        } else {");
        assosciatedCode.addToCorrect("            this.quantity = this.quantity - 1;");
        assosciatedCode.addToCorrect("            System.out.println(\"New quantity is \" + this.quantity);");
        assosciatedCode.addToCorrect("        }");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist			
		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check there are enough lines of code present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 			//pass the test
				
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check the student code against the correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
	
			String runCheck = assosciatedCode.jDoodleTest("").replaceAll("\\\\n", ""); 		//string to hold the output of the code
			
			if ((!(runCheck.contains("/test.java"))) && (runCheck.contains("No more left") || runCheck.contains("New quantity is"))) 		//if there is no error AND one of, no more left or new quantity is ... then passed
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//fail count set to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all tests ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if it has failed increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 		//if no fails then challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}