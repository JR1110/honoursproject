import java.util.TreeMap;

/*
 * Class to hold the tests that have been performed on the students code and the output in terms of pass or fail
 */

public class TestResults {
	
	private TreeMap<String,String> testAndResult; 		//TreeMap to store <testName,result>
	private boolean passed; 							//boolean to say if the entire challenge has been passed
	
	public TestResults() 								//constructor to set initial class with a new TreeMap and setting passed to be false by default
	{
		testAndResult = new TreeMap<String,String>();
		passed = false;
	}
	
	public void resetTestResults() 						//resets the results to their default 
	{
		testAndResult = new TreeMap<String,String>();
		passed = false;
	}
	
	public void addResult(String test, String result) 		//takes in two strings (test and result) and adds them into the map
	{
		this.testAndResult.put(test,result);
	}
	
	//GETTERS AND SETTERS

	public TreeMap<String, String> getTestAndResult() {
		return testAndResult;
	}

	public void setTestAndResult(TreeMap<String, String> testAndResult) {
		this.testAndResult = testAndResult;
	}

	public boolean isPassed() {
		return passed;
	}

	public void setPassed(boolean passed) {
		this.passed = passed;
	}
	
}
