

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/HelloWorldServlet")
public class HelloWorldServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelloWorldServlet() {
        super();
        testResults = new TestResults(); 			//new test results
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
    	testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String code = request.getParameter("code");
		
		/*
		 * THIS CODE WAS DEVELOPED EARLY AND NTO PROGRAMATICALLY ASSESSED DUE TO NOT BEING RELEVANT TO THE SIMPLICITY OF THE CHALLENGE
		 * 
		 * Check for semi colons, correct output phrase and hello world being present
		 * 
		 * if all three are passed then the challenge is passed as so
		 * 
		 */
		
		boolean endingPassed = false;
		boolean structurePassed = false;
		boolean printedWordsPassed = false;
		
		if (code.contains(";"))
		{
			endingPassed = true;
			testResults.addResult("Contains Semi-Colon", "Passed!");
		} else {
			testResults.addResult("Contains Semi-Colon", "Failed");
		}
		
		if (code.contains("System.out.println"))
		{
			structurePassed = true;
			testResults.addResult("Follows Right Structure", "Passed!");
		} else {
			testResults.addResult("Follows Right Structure", "Failed");
		}
		
		if (code.contains("Hello World"))
		{
			printedWordsPassed = true;
			testResults.addResult("Prints \"Hello World\"", "Passed!");
		} else {
			testResults.addResult("Prints \"Hello World\"", "Failed");
		}
		
		if (endingPassed && structurePassed && printedWordsPassed)
		{
			System.out.println("Passed Test!");
			testResults.setPassed(true);
		}
		
		drawPage(request,response,"p");
		
	}
	
	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Hello World</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Hello World</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>As programmers we are very imaginative and creative ... but not when it comes to the examples we use! </p>"
					+ "<p>Hello World is a standard example used in order to see the basics of a new programming language. </p>"
					+ "<P>It's stupidly simple but it allows a quick look at how to word and write a new programming language.</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>Hello World gets the system to print out the words ... \"Hello World\". </p>"
					+ "<p>It really is that simple! It writes those words out to the user using the console or simplest output that the language allows for.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p> To output in Java, the simplest way is to print to the console. The console basically just ouputs anything you tell it to and can be seen in any Java editor, like eclipse.</p>"
					+ "<p>The Java command for writing to the console is - System.out.println();</p><p>It tells the programme to print a new line (println()) to the console (System.out) with the ; signifying the end of the command. <b>In the brackets after println we can put anything we want to show in the console. </b> If it is text we just have to rememeber to put it in quotes!</p>"
					+ "<p>In the box below; write the code to output 'Hello World' to the system console.</p>"
					+ "<form action='' method='post'><input type='text' name='code' placeholder='Enter Code Here!' class='form-control'>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("HelloWorld", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
