

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MaxAndMinServlet
 */
@WebServlet("/MaxAndMinServlet")
public class MaxAndMinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public MaxAndMinServlet() {
        super();
        testResults = new TestResults(); 				//new test results
        assosciatedCode = new assosciatedCode.AssocCode(); 		//new assoc code
        assosciatedCode.addToInitial("int[] values = {65,34,75,12,77,98,64,32,62,44,19,20,43,22,12,34,45,14,98,56,54,76,77,43,25,21,78,67,45,87,86,75,82,91,54,34,97,54,33,17,11};");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Max and Min</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Max and Min</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (Challenge) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Challenge</div><div class='panel-body'>"
					+ "<p>The max and min algortihm takes a list of numbers and finds out the value of both the maximum and minimum number.</p>"
					+ "<p>Whilst normally dealt with in two stages we can combine them into one to save computational, and personal, time.</p>"
					+ "<p>The algorithm is provided below in simple structured english, you should implement it in the code challenge section.</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (Algorithm) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>Algorithm</div><div class='panel-body'>"
					+ "<p>The algorithm is as follows : </p>"
					+ "<ol>"
					+ "<li>Create integer to hold the minimum value (\"minimum\"), set it to the first value</li>"
					+ "<li>Create integer to hold the maximum value (\"maximum\"), set it to the first value</li>"
					+ "<li>For integer 0 till array length</li>"
					+ "<li>&emsp;If current item is lower than \"minimum\"</li>"
					+ "<li>&emsp;&emsp;Set \"minimum\" to current value</li>"
					+ "<li>&emsp;End if</li>"
					+ "<li>&emsp;If current item is higher than \"maximum\"</li>"
					+ "<li>&emsp;&emsp;Set \"maximum\" to current value</li>"
					+ "<li>&emsp;End if</li>"
					+ "<li>End for loop</li>"
					+ "<li>Output the minimum value</li>"
					+ "<li>Output the maximum value</li>"
					+ "</ol>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Implement the above algorithm using your knowledge of data types and computational constructs from earlier!</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='18'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("MaxAndMin", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution in assoc code
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("int[] values = {65,34,75,12,77,98,64,32,62,44,19,20,43,22,12,34,45,14,98,56,54,76,77,43,25,21,78,67,45,87,86,75,82,91,54,34,97,54,33,17,11};");
		assosciatedCode.addToCorrect("int minimum = values[0];");
		assosciatedCode.addToCorrect("int maximum = values[0];");
		assosciatedCode.addToCorrect("for (int i = 0; i < values.length; i++");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("if (values[i] < minimum");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("minimum = values[i];");
		assosciatedCode.addToCorrect("}");
		assosciatedCode.addToCorrect("if (values[i] > maximum");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("maximum = values[i];");
		assosciatedCode.addToCorrect("}");
		assosciatedCode.addToCorrect("}");
		assosciatedCode.addToCorrect("System.out.println(minimum);");
		assosciatedCode.addToCorrect("System.out.println(maximum);");
	}
	
	private void submitCode(String code)
	{
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist

		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//check for enough lines of code
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 		//pass that test
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check against correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
			
			if (assosciatedCode.jDoodleTest("").toString().contains("11\\n98\\n")) 			//check that it outputs the max and min (11 and 98)
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 			//set no. of fails to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 			//for all the tests ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 			//if it has failed, increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if no fails the challenge has been passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
