

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class FindingOnMenuServlet
 */
@WebServlet("/FindingOnMenuServlet")
public class FindingOnMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.OOAssocCode assosciatedCode;

    public FindingOnMenuServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.OOAssocCode(); 		//new assoc code
        
        //add code to the initial code
        assosciatedCode.addToInitial("import java.util.ArrayList;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("public class dish {");
        assosciatedCode.addToInitial("    private String name;");
        assosciatedCode.addToInitial("    private Double price;");
        assosciatedCode.addToInitial("    private String course;");
        assosciatedCode.addToInitial("    private int calories;");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish() {}");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        this.name = name;");
        assosciatedCode.addToInitial("        this.price = price;");
        assosciatedCode.addToInitial("        this.course = course;");
        assosciatedCode.addToInitial("        this.calories = calories;");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToInitial("    public String getName() { return name; }");
        assosciatedCode.addToInitial("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToInitial("    public Double getPrice() { return price; }");
        assosciatedCode.addToInitial("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToInitial("    public String getCourse() { return course; }");
        assosciatedCode.addToInitial("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToInitial("    public int getCalories() { return calories; }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public void outputDish()");
        assosciatedCode.addToInitial("    {");
        assosciatedCode.addToInitial("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial(" ");
        assosciatedCode.addToInitial("    public static void main(String[] args) {");
        assosciatedCode.addToInitial("        ArrayList<dish> menu = new ArrayList<dish>();");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Breaded Brie\",4.50,\"Starter\",560));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Melon\",3.00,\"Starter\",160));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Tomato Soup\",2.50,\"Starter\",310));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Grilled Prawns\",4.20,\"Starter\",400));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Steak and Chips\",18.45,\"Main\",1080));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Fish Pie\",12.35,\"Main\",980));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Cheeseburger\",14.00,\"Main\",1100));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Ceaser Salad\",9.50,\"Main\",580));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Sticky Toffee Pudding\",8.50,\"Desert\",800));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Cheesecake\",7.99,\"Desert\",760));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Sundae\",8.60,\"Desert\",1300));");
        assosciatedCode.addToInitial("        menu.add(new dish(\"Eton Mess\",6.00,\"Desert\",790));");
        assosciatedCode.addToInitial("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToInitial("    }");
        assosciatedCode.addToInitial("}");
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Dealing with Multiple Objects</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Dealing with Multiple Objects</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>The menu has expanded and more dishes have been added to it!</p>"
					+ "<p>One of the things we may want to do is see if there is a dish that we want is in the menu. This can be an issue when making a programe that uses multiple objects of our own class.</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>This builds on the classic algorithm for <a href='SearchingServlet'>searching</a>.</p>"
					+ "<p>If this has not been done then the algorithm for it is provided within that challenge and should be tried before this one!</p>"
					+ "<p>Further to searching, we can also use our class within a for loop in order to go through multiple object held within an array list. By using the variable style of for loop we can go through each of our objects in turn.</p>"
					+ "<p><b>for (dish d : menu)<br>{<br>&emsp;...<br>}</b></p>"
					+ "<p>Within the for loop we can use d as a temporary dish and use it in a variety of ways, such as if statements or adding up all the prices of a dish. This is the same for any class we make.</p>"
					+ "<p>This is useful, but not required for the code challenge below!</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>By using the algorithm provided in the searching challenge, find out if the restaurant serves a dish of your choice.</p>"
					+ "<p>Search for this by performing an if statement on the dish name equals the string of your chosen dish.</p>"
					+ "<p>HINT: this can be done by using menu.get(counter).getName().equals(desiredItem)</p>"
					+ "<p>If found, output <b>desiredItem is on the menu</b></p>"
					+ "<p>If not found, output <b>desiredItem is not on the menu</b></p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='40'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("FindingOnMenu", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//add code to the correct answer
	private void correctSolution()
	{
        assosciatedCode.addToCorrect("import java.util.ArrayList;");
        assosciatedCode.addToCorrect(" ");
		assosciatedCode.addToCorrect("public class dish {");
        assosciatedCode.addToCorrect("    private String name;");
        assosciatedCode.addToCorrect("    private Double price;");
        assosciatedCode.addToCorrect("    private String course;");
        assosciatedCode.addToCorrect("    private int calories;");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish() {}");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public dish(String name, Double price, String course, int calories)");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        this.name = name;");
        assosciatedCode.addToCorrect("        this.price = price;");
        assosciatedCode.addToCorrect("        this.course = course;");
        assosciatedCode.addToCorrect("        this.calories = calories;");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" has been created\");");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void setName(String name) { this.name = name; }");
        assosciatedCode.addToCorrect("    public String getName() { return name; }");
        assosciatedCode.addToCorrect("    public void setPrice(Double price) { this.price = price; }");
        assosciatedCode.addToCorrect("    public Double getPrice() { return price; }");
        assosciatedCode.addToCorrect("    public void setCourse(String course) { this.course = course; }");
        assosciatedCode.addToCorrect("    public String getCourse() { return course; }");
        assosciatedCode.addToCorrect("    public void setCalories(int calories) { this.calories = calories; }");
        assosciatedCode.addToCorrect("    public int getCalories() { return calories; }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public void outputDish()");
        assosciatedCode.addToCorrect("    {");
        assosciatedCode.addToCorrect("        System.out.println(this.name + \" costs \" + this.price);");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect(" ");
        assosciatedCode.addToCorrect("    public static void main(String[] args) {");
        assosciatedCode.addToCorrect("        ArrayList<dish> menu = new ArrayList<dish>();");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Breaded Brie\",4.50,\"Starter\",560));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Melon\",3.00,\"Starter\",160));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Tomato Soup\",2.50,\"Starter\",310));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Grilled Prawns\",4.20,\"Starter\",400));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Steak and Chips\",18.45,\"Main\",1080));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Fish Pie\",12.35,\"Main\",980));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Cheeseburger\",14.00,\"Main\",1100));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Ceaser Salad\",9.50,\"Main\",580));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Sticky Toffee Pudding\",8.50,\"Desert\",800));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Cheesecake\",7.99,\"Desert\",760));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Sundae\",8.60,\"Desert\",1300));");
        assosciatedCode.addToCorrect("        menu.add(new dish(\"Eton Mess\",6.00,\"Desert\",790));");
        assosciatedCode.addToCorrect("        /*ENTER YOUR CODE BELOW HERE*/");
        assosciatedCode.addToCorrect("        String desiredItem = \"");
        assosciatedCode.addToCorrect("        boolean found = false;");
        assosciatedCode.addToCorrect("        int counter = 0;");
        assosciatedCode.addToCorrect("        do");
        assosciatedCode.addToCorrect("        {");
        assosciatedCode.addToCorrect("            if(menu.get(counter).getName().equals(desiredItem))");
        assosciatedCode.addToCorrect("            {");
        assosciatedCode.addToCorrect("                System.out.println(desiredItem + \" is on the menu\");");
        assosciatedCode.addToCorrect("                found = true;");
        assosciatedCode.addToCorrect("            }");
        assosciatedCode.addToCorrect("            counter = counter + 1;");
        assosciatedCode.addToCorrect("        } while(counter < menu.size() || found == true);");
        assosciatedCode.addToCorrect("        if (found == false)");
        assosciatedCode.addToCorrect("        {");
        assosciatedCode.addToCorrect("            System.out.println(desiredItem + \" is not on the menu\");");
        assosciatedCode.addToCorrect("        }");
        assosciatedCode.addToCorrect("    }");
        assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist			
		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//check for the right amount of code present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
				
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check that the current line matches the correct asnwer one
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
	
			String runCheck = assosciatedCode.jDoodleTest("").replaceAll("\\\\n", ""); 		//string to hold the output of the code
			
			if ((!(runCheck.contains("/test.java"))) && runCheck.contains("on the menu")) 		//check the output is not an error and contains on the menu
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to be 0
			
			for (String s : testResults.getTestAndResult().keySet()) 				//for all the tests ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 				//if it has failed then increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 		// if there are no fails then the challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
