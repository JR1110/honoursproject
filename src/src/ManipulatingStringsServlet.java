

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/ManipulatingStringsServlet")
public class ManipulatingStringsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public ManipulatingStringsServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.AssocCode(); 			//new assoc code
        assosciatedCode.addToInitial("String x = \"I can code\";"); 				//adding the initial to be completed 
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student
		
		testResults.resetTestResults();
		
		submitCode(code);			 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}
	
	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Manipulating and Altering Strings</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Storing Information in Strings</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Now we know how to use strings we can explore how powerful they can be.</p>"
					+ "<p>There are multiple ways to manipulate strings, with the ability to alter both the appearance as well as what the strings contain.</p>"
					+ "<p>We will explore more of these below so don't worry if it sounds complicated!</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>To explore string manipulation we will look at the code and effect on the following string - String str = \"Hello World\"; </p>"
					+ "<p>All of the following could be outputted as we have previously, System.out.println(str.*); - the * being one of the manipulations discussed below</p>"
					+ "<table class='table table-hover table-striped'> <thead><tr><th>Action</th><th>Manipulation (*)</th><th>Result (System.out.println(*))</th></tr></thead><tbody>"
					+ "<tr><td>Turn it all upper case</td><td>str.toUpperCase()</td><td>HELLO WORLD</td></tr>"
					+ "<tr><td>Or all lower case</td><td>str.toLowerCase()</td><td>hello world</td></tr>"
					+ "<tr><td>Find out how many characters a string has</td><td>str.length()</td><td>11</td></tr>"
					+ "<tr><td>Replace specific character (character to find, character to replace with)</td><td>str.replace(\" \", \"-\")</td><td>Hello-World</td></tr>"
					+ "<tr><th colspan='3'>Substrings are very powerful as they let you access parts of the string at a time</th></tr>"
					+ "<tr><td>First you can show characters after a position (position to read from)</td><td>str.substring(8)</td><td>'rld'</td></tr>"
					+ "<tr><td>Or you can select a start and end point (position to read from, how many characters to read)</td><td>str.substring(0,2)</td><td>'He'</td></tr>"
					+ "<tr><td>You can group these all in java</td><td>str.toUpperCase().replace(\" \", \"-\").substring(4);</td><td>O-WORLD</td></tr></tbody></table>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Now that we know about these manipulations of string, lets try it for ourselves!</p>"
					+ "<p>Adapt String x.</p>"
					+ "<p>Output the length of the string.</p>"
					+ "<p>Then <b>in one line</b> output the string in lower case and display only the first 3 characters</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("ManipulatingStrings", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding code to the stored correct answer
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("String x = \"I can code\"");
		assosciatedCode.addToCorrect("System.out.println(x.length());");
		assosciatedCode.addToCorrect("System.out.println(x.toLowerCase().substring(0,3));");
	}
	
	private void submitCode(String code)
	{
		int linesOfCode = 0; 									//int to hold the number of lines of code, not start like comment
		int semiColons = 0; 								//int to check how many semi colons have been entered
		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist
			
			if(!(s.contains("//") && (!(s.contains(";"))))) 		//if the line begins with a comment 
			{
				linesOfCode++;
			}
			
			if (s.contains(";")) 		//if it contains a semi colon
			{
				semiColons++; 								//adds one on to the semi colon count
			}
		}
		
		if (semiColons == linesOfCode) 			//if there are the same number of semi colons as lines of code
		{
			testResults.addResult("Contains Semi-Colons", "Passed!"); 		//adding the test result that the code contains relevant semi colons
		} else {
			testResults.addResult("Contains Semi-Colons", "Failed"); 		//adding the test results that it failed the semi colon check
		}

		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check for enough lines of code
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 		//passed the test
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check current line against correct answer
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
			
			if(assosciatedCode.jDoodleTest("").toString().equals("10\\ni c\\n")) 			//checks for the length and first three characters to be outputted
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for each test
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if it has failed then increase fial count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if 0 fails then challenge is passed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
	

}
