

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class IfStatementServlet
 */
@WebServlet("/IfStatementServlet")
public class IfStatementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public IfStatementServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.AssocCode();			//new assoc code
        assosciatedCode.addToInitial("int x = 4;"); 				//adding the initial to be completed 
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>If Statements</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>If Statements</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>If statements are one of the most important computational constructs we have in programming. The use of an if statement can allow us to make decisions based on data of all types.</p>"
					+ "<p>They allow us to compare and contrast data such as inputs, equation results and boolean data</p>"
					+ "<p>We can also build on the standard if statement by using if, else or else-if's</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>If statements are set out by using <b>if(){}</b> with the checks being carried out within the brackets and the actions within the curly brackets.</p>"
					+ "<p>The way we compare items is different depending on what it is we want to compare. We however have to compare like for like, e.g. string to string or integer to integer</p>"
					+ "<p>Comparison operators vary by the data we are comparing. Assume x and y are of the data types we are comparing.</p>"
					+ "<table class = 'table table-hover table-striped'><thead><tr><th>Comparing</th><th>Code Example</th><th>Comment</th></thead>"
					+ "<tbody><tr><td>Strings</td><td>if(x.equals(y))</td><td>Check two strings equal eachother</td></tr>"
					+ "<tr><td>Strings</td><td>if(x.equals(\"this\"))</td><td>Check a string is equal to your choice of string</td></tr>"
					+ "<tr><td>Strings</td><td>if(x.contains(y))</td><td>Check if string x contains string y anywhere in it</td></tr>"
					+ "<tr><td>Strings</td><td>if(!(x.equals(y))</td><td>Check if two strings don't equal eachother. The ! means decide on the oppoite of the statement without it</td></tr>"
					+ "<tr><td>Integer</td><td>if(x == y)</td><td>Check two integers equal eachother</td></tr>"
					+ "<tr><td>Integer</td><td>if(x == 3)</td><td>Check if an integer is equal to your choice of number</td></tr>"
					+ "<tr><td>Integer</td><td>if(x < y)</td><td>Check if x is less than y</td></tr>"
					+ "<tr><td>Integer</td><td>if(x > y)</td><td>Check if x is more than y</td></tr>"
					+ "<tr><td>Integer</td><td>if(x != y)</td><td>Check if x is not equal to y</td></tr>"
					+ "<tr><td>Boolean</td><td>if(x == true)</td><td>Check if a boolean is set to true</td></tr>"
					+ "<tr><td>Boolean</td><td>if(x == false)</td><td>Check if a boolean is set to false</td></tr>"
					+ "</tbody></table>"
					+ "<p>A completed if statement would like this, String x = \"string\";</p>"
					+ "<p>if(x.equals(\"string\"))<br>{<br>&emsp;System.out.println(\"Matches\");<br>} else { <br>&emsp;System.out.println(\"Does not match\");<br>}</p>"
					+ "<p>This would result in the output of Matches</p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>We will create an if statement on int x.</p>"
					+ "<p>Create an if/else statement that will output \"Matches\" or \"Does not match\" based on the integer.</p>"
					+ "<p>The if statement can compare it in any way as long as the output results as \"Does not match\"</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("IfStatements", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("int x = 4;");
		assosciatedCode.addToCorrect("if (x");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("System.out.println(\"Matches\");");
		assosciatedCode.addToCorrect("} else {");
		assosciatedCode.addToCorrect("System.out.println(\"Does not match\");");
		assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist

		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 			//check enough lines are present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!");
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check students code against correct answer bar spaces
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
			
			if (assosciatedCode.jDoodleTest("").toString().contains("Does not match")) 				//if the output is does not match then it is passed
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all tests run
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if it is failed then increase the fail count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if no failures then pass the challenge
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
