

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class StoringStringsServlet
 */
@WebServlet("/StoringStringsServlet")
public class StoringStringsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public StoringStringsServlet() {
        super();
        testResults = new TestResults(); 							//create new test results class item
        assosciatedCode = new assosciatedCode.AssocCode(); 			//creating a new assosciated code class 
        assosciatedCode.addToInitial("String x = "); 				//adding the initial to be completed 
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Storing Information In Strings</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Storing Information in Strings</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (why) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Why?</div><div class='panel-body'>"
					+ "<p>Strings are the most basic way of storing any form of information.</p>"
					+ "<p>We mainly use strings to store text based information and they have many different abilities to manipulate and edit text. However we can store any piece of data in a string, numbers letters or otherwise. Storing numbers in strings can be quick and easy but they can't be treat as numbers simply just a piece of text with a number in it</p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (what) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>What?</div><div class='panel-body'>"
					+ "<p>Strings can store multiple characters in a row, how we store words sentances or codes.</p>"
					+ "<p>All programming languages have strings no matter how old or new, they are the most basic and needed data type.</p>"
					+ "<p>The most common ways to use strings are to set them to be a bit of text such as : <b>String name = \"Josh\";</b></p>"
					+ "<p>But they could be set to be null : <b>String name = null;</b> With the string being used later on in the programme to take the information : <b>name = \"Josh\";</b></p>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>We will build on our hello world example to make it more complex ... but only just!</p>"
					+ "<p>Complete String x which should hold the text to be outputed (\"Hello World\" if you can't remember).</p>"
					+ "<p>Then output the string to the console as shown in the Hello World example.</p>"
					+ "<p>As we're outputting a string this time we wont need the quote marks in the output brackets simply the name of the String.</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='6'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("StoringInfoInStrings", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding the correct solution to the test results class
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("String x = 'Hello World';");
		assosciatedCode.addToCorrect("System.out.println(x);");
	}
	
	private void submitCode(String code)
	{
		int linesOfCode = 0; 									//int to hold the number of lines of code, not start like comment
		int semiColons = 0; 								//int to check how many semi colons have been entered
		
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("\"", "'")); 										//add it to the arraylist
			
			if(!(s.contains("//") && (!(s.contains(";"))))) 		//if the line begins with a comment 
			{
				linesOfCode++;
			}
			
			if (s.contains(";")) 		//if it contains a semi colon
			{
				semiColons++; 								//adds one on to the semi colon count
			}
		}
		
		if (semiColons == linesOfCode) 			//if there are the same number of semi colons as lines of code
		{
			testResults.addResult("Contains Semi-Colons", "Passed!"); 		//adding the test result that the code contains relevant semi colons
		} else {
			testResults.addResult("Contains Semi-Colons", "Failed"); 		//adding the test results that it failed the semi colon check
		}

		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//if enough lines of code are present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 		//add that as a successful test
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check code without any spaces 
				{
					testResults.addResult("Line " + i + " correct", "Passed!"); 		//if they match; pass
				} else {
					testResults.addResult("Line " + i + " correct", "Failed"); 			//if they dont match; fail
				}
			}
			
			if (assosciatedCode.jDoodleTest("").toString().contains("Hello World")) 		//if the output from JDoodle is hello world
			{
				testResults.addResult("Code Runs", "Passed!"); 		
			} else {
				testResults.addResult("Code Runs", "Failed"); 		
			}
			
			int failCount = 0; 		//check how many items have failed in testing
			
			for (String s : testResults.getTestAndResult().keySet()) 		//for all the strings in the tests ran
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 		//if they are failed add one to the count
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 			//if none have failed
			{
				testResults.setPassed(true); 			//set the challenge to passed overall
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}
