

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SearchingServlet
 */
@WebServlet("/SearchingServlet")
public class SearchingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TestResults testResults;
	private assosciatedCode.AssocCode assosciatedCode;
       
    public SearchingServlet() {
        super();
        testResults = new TestResults(); 							//new test results
        assosciatedCode = new assosciatedCode.AssocCode(); 			//new assosciated code
        assosciatedCode.addToInitial("String[] names = {\"Olivia\",\"Amelia\",\"Emily\",\"Isla\",\"Ava\",\"Oliver\",\"Harry\",\"George\",\"Jack\",\"Jacob\"};"); 		//adding to the initial challenge code
        correctSolution(); 						//enter the correct solution
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		testResults.setPassed(false); 		//resets the pass to false
    	drawPage(request,response,"g"); 			//draws the page
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		String code = new String(request.getParameter("code")); 		//getting the code entered bv the student

		testResults.resetTestResults(); 		//resets the test results
			
		submitCode(code); 			//run the submit code sub routine
		
		runFullTests(); 						//runs all the tests
		
		drawPage(request,response,"p"); 		//draw the page again but with results
	}

	private void drawPage(HttpServletRequest request, HttpServletResponse response, String mode)
	{		
		try {
			response.setContentType("text/html"); 				//setting the response to write html back
			
			PrintWriter writer = response.getWriter(); 			//setting up the writer
			
			//Writing out the HEAD section
			writer.println("<!DOCTYPE html><html><head><meta charset='UTF-8'>"
					+ "<title>Searching</title>"
					+ "<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>"
					+ "<link rel='stylesheet' href='custom.css'>"
					+ "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>"
					+ "<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js'></script>"
					+ "</head>");

			//Writing the start of the body
			writer.println("<body><div class='container'><h2>Searching</h2>"); 		
			
			//writing the panels start
			writer.println("<div class='panel-group'>");
			
			//writing first (Challenge) panel
			writer.println("<div class='panel panel-default panel-collapse' id='initialInfo'>"
					+ "<div class='panel-heading'>Challenge</div><div class='panel-body'>"
					+ "<p>The searching algorithm loops through an array in order to find out if it contains a certain element.</p>"
					+ "<p>We use a special type of loop called a Do While, which performs an action whilst some attributes are true.</p>"
					+ "<p>Using a do while loop is better in this algorithm because it allows us to stop the processing as soon as we find the item we are looking for.</p>"
					+ "<p>To make a loop like this in java we use <b><br>do<br>{<br>&emsp;Code to run<br>} while(some condition is not met);</b></p></div>"
					+ "<div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#furtherInfo'>Read Block</button></div></div>"); 
			
			//writing second (Algorithm) panel
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='furtherInfo'>"
					+ "<div class='panel-heading'>Algorithm</div><div class='panel-body'>"
					+ "<p>The algorithm is as follows : </p>"
					+ "<ol>"
					+ "<li>Create and set \"desiredItem\" to be searched against (string, int, etc.)</li>"
					+ "<li>Create boolean \"found\" and set it to be false</li>"
					+ "<li>Create integer \"counter\" and set it to be 0</li>"
					+ "<li>do</li>"
					+ "<li>&emsp;If item at counter's value is equal to the search value</li>"
					+ "<li>&emsp;&emsp;Output \"desiredItem\" has been found at position \"counter\"</li>"
					+ "<li>&emsp;&emsp;Set \"found\" to be true</li>"
					+ "<li>&emsp;End if</li>"
					+ "<li>&emsp;Set \"counter\" to be \"counter\" + 1</li>"
					+ "<li>While \"counter\" < length of the array to search through OR found is true</li>"
					+ "<li>If \"found\" is false</li>"
					+ "<li>&emsp;Output \"desiredItem\" has not been found</li>"
					+ "<li>End if</li>"
					+ "</ol>"
					+ "</div><div class='panel-footer'>"
					+ "<button type='button' class='btn btn-info' data-toggle='collapse' data-target='#codeChallenge'>Read Block</button></div></div>");
						
			//writing third panel (code challenge!)			
			writer.println("<div class='panel panel-default panel-collapse ");
			if (mode.equals("g"))
			{
				writer.print("collapse");
			}
			writer.print("' id='codeChallenge'>"
					+ "<div class='panel-heading'>Code Challenge</div><div class='panel-body'>"
					+ "<p>Implement the above algorithm to see if your name is in the top 5 names of 2017.</p>"
					+ "<p>HINT : to output both variables and text we use the plus within the output statement. e.g. System.out.println(desiredItem + \" has been found at \" + counter + \" position\");</p>"
					+ "<p>HINT : In java we use || to say OR in an if or while statement.</p>"
					+ "<form action='' method='post'><textarea name='code' placeholder='Enter Code Here!' class='form-control text' rows='18'>");
			
			if (mode.equals("p")) 		//if it is accessed by POST and thus has student code 
			{
				//gets the already entered student code
				for (String s: assosciatedCode.getStudentAnswer())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			} else  { 				 
				//for the arraylist of strings containing the initial code for the student
				for (String s: assosciatedCode.getInitialCode())
				{
					//write it in the textarea followed by a new line
					writer.print(s+'\n');
				}
			}
			
			writer.print("</textarea>"
					+ "<br>	<input type='submit' class='btn btn-default' value='Test Code!'></form></div>");
			
			
			//writing the results if coming from post
			if (mode.equals("p"))
			{
				writer.println("<table id='testResults' class='table table-condensed'>");
				
				//for all the results print their status
				for(String s : testResults.getTestAndResult().keySet())
				{
					if (testResults.getTestAndResult().get(s).equals("Passed!"))
					{
						writer.println("<tr class='success'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					} else {
						writer.println("<tr class='danger'><th>" + s + "</th><td>" + testResults.getTestAndResult().get(s) + "</td></tr>");
					}
				}
				
				writer.println("</table>");
			}
			
			//Writing if passed next options	
			writer.println("<div class='panel-footer'>");
			
			if (testResults.isPassed() == true)
			{
				writer.println("<a href='SectionSelecterServlet' class='btn btn-default'>Section Selector</a>");
				HttpSession session = request.getSession();
				session.setAttribute("Searching", "Passed!");
			}
			
			//writing end of HTML
			writer.println("</div></div></div></div></body></html>");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//adding to the correct solution
	private void correctSolution()
	{
		assosciatedCode.addToCorrect("String[] names = {\"Olivia\",\"Amelia\",\"Emily\",\"Isla\",\"Ava\",\"Oliver\",\"Harry\",\"George\",\"Jack\",\"Jacob\"};");
		assosciatedCode.addToCorrect("String desiredItem = \"");
		assosciatedCode.addToCorrect("boolean found = false;");
		assosciatedCode.addToCorrect("int counter = 0;");
		assosciatedCode.addToCorrect("do");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("if (names[counter].equals(desiredItem))");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("System.out.println(desiredItem + \" has been found at position \" + counter);");
		assosciatedCode.addToCorrect("found = true;");
		assosciatedCode.addToCorrect("}");
		assosciatedCode.addToCorrect("counter = counter + 1;");
		assosciatedCode.addToCorrect("} while (counter < names.length || found == true);");
		assosciatedCode.addToCorrect("if (found == false)");
		assosciatedCode.addToCorrect("{");
		assosciatedCode.addToCorrect("System.out.println(desiredItem + \" has not been found \");");
		assosciatedCode.addToCorrect("}");
	}
	
	private void submitCode(String code)
	{
		String[] codeLines = code.split("\n"); 			//split the code into the individual lines
		
		ArrayList<String> studentAnswer = new ArrayList<String>(); 		//new array list string to go back to the store strings class
		for (String s : codeLines) 										//for each of the strings in code lines
		{
			studentAnswer.add(s.replace("'", "\"")); 										//add it to the arraylist

		}
		
		assosciatedCode.setStudentAnswer(studentAnswer); 				//setting the student answer to be the student answer arraylist
		
	}
	
	private void runFullTests()
	{
		if (assosciatedCode.getCorrectAnswer().size() == assosciatedCode.getStudentAnswer().size()) 		//if enough lines are present
		{
			testResults.addResult("Correct Number Of Lines", "Passed!"); 		//pass that test
			
			for (int i = 0; i < assosciatedCode.getCorrectAnswer().size(); i++) 			//i = 1 as first line is always a comment
			{
				if (assosciatedCode.getStudentAnswer().get(i).replaceAll(" ", "").contains(assosciatedCode.getCorrectAnswer().get(i).replaceAll(" ", ""))) 		//check student code against corrcet answer bar spaces
				{
					testResults.addResult("Line " + i + " correct", "Passed!");
				} else {
					testResults.addResult("Line " + i + " correct", "Failed");
				}
			}
			
			String runCheck = assosciatedCode.jDoodleTest("").replaceAll("\\\\n", ""); 		//string to hold the output of the code
			
			if (!(runCheck.contains("/test.java"))) 				//if it does not contain the error code from JDoodle then pass OR fail
			{
				testResults.addResult("Code Runs", "Passed!");
			} else {
				testResults.addResult("Code Runs", "Failed");
			}
			
			int failCount = 0; 		//set fail count to 0
			
			for (String s : testResults.getTestAndResult().keySet()) 				//for all the tests passed
			{
				if (testResults.getTestAndResult().get(s) == "Failed") 				//if the pass has failed then add one to the counter
				{
					failCount++;
				}
			}
			
			if (failCount == 0) 						//if no tests have failed then the whole challenge is completed
			{
				testResults.setPassed(true);
			}
		} else {
			testResults.addResult("Correct Number Of Lines", "Failed");
		}
		
	}
}