This is the readme alongisde the application for  the following project :

Levelling the Playing Field:
A Web Application to Allow All First Year Students to Learn Programming Successfully

To run, this should be run off of a Tomcat 9.0 server and uses Java EE. This is best done through the eclipse EE Editor which combines the two technologies. 
From here the localhost/src/sectionSelectorServlet can be accessed to select challenges.

If you desire to set a username for the seesion then simply load to the localhost/src/ page and this is possible from there.